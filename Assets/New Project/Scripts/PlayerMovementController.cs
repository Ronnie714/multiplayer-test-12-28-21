﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    PlayerInputController pic;
    Rigidbody rb;
    float speed;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pic = GetComponent<PlayerInputController>();
        speed = 70;
    }


    void FixedUpdate()
    {
        if (pic.Right) {
            rb.AddForce(new Vector3(1, 0, 0) * speed, ForceMode.Force);
        }
        if (pic.Left)
        {
            rb.AddForce(new Vector3(-1, 0, 0) * speed, ForceMode.Force);
        }
        if (pic.Up)
        {
            rb.AddForce(new Vector3(0, 0, 1) * speed, ForceMode.Force);
        }
        if (pic.Down)
        {
            rb.AddForce(new Vector3(0, 0, -1) * speed, ForceMode.Force);
        }
    }
}
