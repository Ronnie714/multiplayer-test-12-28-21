﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using System;

public class PlayerInputController : MonoBehaviour
{
    public PlayerInformation info;
    public bool Right { get; private set; }
    public bool Left { get; private set; }
    public bool Up { get; private set; }
    public bool Down { get; private set; }


    public struct DataPoint
    {
        public bool Right;
        public bool Left;
        public bool Up;
        public bool Down;
    }


    void Start()
    {
        
    }


    [Command]
    public void updateServerControls(DataPoint point)
    {
        Right = point.Right;
        Left = point.Left;
        Up = point.Up;
        Down = point.Down;
        Debug.Log("TEST: " + point.Right + " " + point.Left);
    }

    void Update()
    {
        //if (CustomNetworkManager.GetInstance().isServer)
        //{
            //if(info.type == PlayerType.Local)
            //{
                Right = Input.GetKey("d");
                Left = Input.GetKey("a");
                Up = Input.GetKey("w");
                Down = Input.GetKey("s");
            //}
        //}
        //else
        //{
            //if (CustomNetworkManager.GetInstance().isConnected && info.PlayerID == 1)
            /*{
                DataPoint point;
                point.Right = Input.GetKey("d");
                point.Left = Input.GetKey("a");
                point.Up = Input.GetKey("w");
                point.Down = Input.GetKey("s");
                updateServerControls(point);
            }


        }*/
    }
}
