﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType
{
    Remote,
    Local
}

public class PlayerInformation : MonoBehaviour
{
    public int PlayerID;
    public int TypeID;
    public PlayerType type;


}
