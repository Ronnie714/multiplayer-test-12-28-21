﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

public class CustomNetworkManager : NetworkManager
{
    public static CustomNetworkManager instance;

    public GameObject PrefabBank, PlayersFolder, PlayersInfoFolder, SpawnPointsFolder;
    public List<GameObject> players { get; private set; }

    public static CustomNetworkManager GetInstance()
    {
        return instance;
    }

    public bool isServer = false;
    public bool isConnected = false;
    private GameObject manager;

    bool first = true;

    public override void OnStartServer()
    {
        isServer = true;

    }
    public override void OnStopServer()
    {
        isServer = false;
    }
    public override void OnClientConnect(NetworkConnection conn)
    {
        isConnected = true;
        ClientScene.Ready(conn);
    }
    public override void OnServerConnect(NetworkConnection conn)
    {
        if (first)
        {
            first = false;
        }
        else
        {
            players = new List<GameObject>();
            foreach (Transform t in PlayersInfoFolder.transform)
            {
                Debug.Log(t.gameObject);
                spawnPlayer(t.GetComponent<PlayerInformation>());
            }

            if (CustomNetworkManager.GetInstance().isServer)
            {
                if (players.Count >= 2)
                {
                    GameObject g = players[1];
                    g.GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
                }
            }
        }
    }
    public override void OnClientDisconnect(NetworkConnection conn)
    {
        isConnected = false;
    }



    private new void Awake()
    {
        if (instance == null)
        {
            manager = GameObject.Find("Manager");
            instance = this;
        }
        else
        {
            throw (new Exception("There should only be one network manager bruh..."));
        }


        base.Awake();
    }

    private void spawnPlayer(PlayerInformation pi)
    {
        if (CustomNetworkManager.GetInstance().isServer)
        {
            GameObject p = Instantiate(PrefabBank.GetComponent<PrefabBank>().playerTypes[pi.TypeID]);
            p.GetComponent<PlayerInputController>().info = pi;
            p.transform.parent = PlayersFolder.transform;
            p.transform.position = SpawnPointsFolder.transform.GetChild(pi.PlayerID).transform.position;
            players.Add(p);
            NetworkServer.Spawn(p);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
