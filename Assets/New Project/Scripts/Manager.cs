﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public GameObject PrefabBank, PlayersFolder, PlayersInfoFolder, SpawnPointsFolder;
    public List<GameObject> players { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        players = new List<GameObject>();
        foreach (Transform t in PlayersInfoFolder.transform)
        {
            Debug.Log(t.gameObject);
            spawnPlayer(t.GetComponent<PlayerInformation>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void spawnPlayer(PlayerInformation pi)
    {
        //if (CustomNetworkManager.GetInstance().isServer)
        //{
            GameObject p = Instantiate(PrefabBank.GetComponent<PrefabBank>().playerTypes[pi.TypeID]);
            p.GetComponent<PlayerInputController>().info = pi;
            p.transform.parent = PlayersFolder.transform;
            p.transform.position = SpawnPointsFolder.transform.GetChild(pi.PlayerID).transform.position;
            players.Add(p);
            NetworkServer.Spawn(p);
        //}

    }
}
